### Infrastructure as code
___

##### Templates use terraform and ansible to configure servers

web server template: git, mc, docker-ce, docker-compose, nginx, certbot

For deploy use:
```
terraform init
terraform apply
```


SSH-keys folder for add keys to DC Hetzner

Terraform provider Hetzner:  https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs
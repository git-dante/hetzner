# Define Hetzner provider
provider "hcloud" {
  token = "${var.hcloud_token}"
}

# Obtain ssh key data
data "hcloud_ssh_key" "key1" {name = "CUP"}
data "hcloud_ssh_key" "key2" {name = "Taylakov"}
data "hcloud_ssh_key" "key3" {name = "Gitlab"}

data "hcloud_ssh_keys" "all_keys" {}

# Create server
resource "hcloud_server" "web" {
  name        = "${var.vm_name}"
  image       = "${var.image_name}"
  server_type = "${var.server_type}"
  ssh_keys    = [
    "${data.hcloud_ssh_key.key1.id}",
    "${data.hcloud_ssh_key.key2.id}",
    "${data.hcloud_ssh_key.key3.id}"
]
  labels = {
     module = "preprod"
     email = "admin_at_joy_money"
}
}

output "public_ip" {
  value = "${hcloud_server.web.ipv4_address}"
}

#---------aws--------

provider "aws" {
  region     = "us-west-2"
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
}

data "aws_route53_zone" "selected" {
  name         = "${var.dns_zone}"
  private_zone = false
}

resource "aws_route53_record" "backend" {
  zone_id = "${data.aws_route53_zone.selected.zone_id}"
  name    = "${var.vm_name}.${data.aws_route53_zone.selected.name}"
  type    = "A"
  ttl     = "300"
  records = ["${hcloud_server.web.ipv4_address}"]
  allow_overwrite = true

provisioner "local-exec" {
   command = "echo [web] > ./ansible/inventory; echo \"  ${self.name}\" >> ./ansible/inventory; echo site_url: \"${self.name}\" > ./ansible/variables"
  }
provisioner "local-exec" {
    command = "ansible-playbook -i inventory main.yaml"
    working_dir = "ansible"
  }
}

variable "vm_name" {
# type = string
#  type = "list"
#  default = ["node1", "node2", "node3"]
}

variable "image_name" {}

variable "server_type" {}

variable "aws_access_key" {}

variable "aws_secret_key" {}

#variable "public_address" {
#default = "1.2.3.4"
#}

# Token variable
variable "hcloud_token" {}

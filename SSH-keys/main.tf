# Define Hetzner provider
provider "hcloud" {
  token = "${var.hcloud_token}"
}

# Create a new SSH key
resource "hcloud_ssh_key" "cup_key" {
  name = "CUP"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

resource "hcloud_ssh_key" "mac_key" {
  name = "Taylakov_macbook"
  public_key = "${file("~/DEVOPS/ansible/user_keys/taylakov_is@macbook")}"
}

resource "hcloud_ssh_key" "my_key" {
  name = "Taylakov"
  public_key = "${file("~/DEVOPS/ansible/user_keys/taylakov_is@10.10.10.137")}"
}

resource "hcloud_ssh_key" "gitlab_key" {
  name = "Gitlab"
  public_key = "${file("~/DEVOPS/ansible/user_keys/deploy@gitlab.joy.money")}"
}
